export class Address {
    idAddress: number;
    street: string;
    province: string;
    country: string;
    city: string;
    floor: string;
    door: string;
    // private client: Client;
    default: boolean;
}

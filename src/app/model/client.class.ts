import { Address } from './address.class';

export class Client {
  idClient: number;
  name: string;
  surname: string;
  documentId: string;
  clientType: number;
  address: Address[];

  constructor(parameters) { }
}

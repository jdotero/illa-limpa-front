import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';

@Injectable()
export class LinkerService {

  private restBase = environment.restBase;

  constructor(private http: HttpClient) { }

  getEntity() {
    return this.http.get(this.restBase + 'users');
  }
}

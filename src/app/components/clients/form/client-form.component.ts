import { Component, OnInit } from '@angular/core';
import { Client } from '../../../model/client.class';

@Component({
    selector: 'app-client-form',
    templateUrl: './client-form.component.html',
    styleUrls: []
})
export class ClientFormComponent implements OnInit {
    model: Client;
    submitted = false;
    constructor() { }

    ngOnInit() { }

    onSubmit() { this.submitted = true; }

    // Delete on production
    get diagnostic() { return JSON.stringify(this.model); }
}

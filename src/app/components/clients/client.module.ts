import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ClientComponent } from './client.component';
import { ClientFormComponent } from './form/client-form.component';
import { ClientListComponent } from './list/list.component';

@NgModule({
  declarations: [
    ClientComponent,
    ClientFormComponent,
    ClientListComponent
  ],
  imports: [ CommonModule ],
  exports: [],
  providers: [],
})
export class ClientModule {}

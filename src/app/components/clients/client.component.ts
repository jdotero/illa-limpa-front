import { Component } from '@angular/core';
import { ClientFormComponent } from './form/client-form.component';
import { ClientListComponent } from './list/list.component';

@Component({
  selector: 'app-client',
  templateUrl: './client.component.html',
  styleUrls: []
})
export class ClientComponent {
  constructor() { }

}

import { Component, Injectable } from '@angular/core';
import { LinkerService } from './core/services/linker.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'apps';

  constructor(private linkerService: LinkerService) {
  }
}
